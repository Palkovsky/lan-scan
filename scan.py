import socket
import struct
import ipaddress
import random
import asyncio
import os
import sys
import time

from pingers import icmp, udp, tcp


'''
    addresses(host, mask)
    This generator iterates over all addresses in network, excluding broadcast and network.
'''
def networkAddresses(host, mask):
    mask = ipaddress.IPv4Address(mask)
    host = ipaddress.IPv4Address(host)
    net  = ipaddress.IPv4Address(int(mask) & int(host))

    for off in range(int(ipaddress.IPv4Address('255.255.255.255')) - int(mask) + 1):
        addr = str(net + off)
        yield addr

'''
    defaultMask(octet)
    Returns default subnet mask for givent first octet.
'''
def guessSubnetMask(addr):
    octet = int(addr.split('.')[0])
    ranges = [0, 126, 128, 191, 192, 223]
    bytes = 1
    for i in range(0, len(ranges)-1, 2):
        if octet >= ranges[i] and octet <= ranges[i+1]:
            break
        bytes += 1
    if bytes > 3:
        return ''
    return ('255.' * bytes + '0.' * (4-bytes))[:-1]

'''
    resolveHostAddress()
    Tries to resolve host address in the network.
'''
def resolveHostAddress():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    ip = s.getsockname()[0]
    s.close()
    return ip

'''
    scan(...)
    Pings every host in current network. Returns array of responding address.
'''
async def scan(pinger, mask, ip = None):
    ip = ip if ip else resolveHostAddress()

    coroutines = [pinger(loop, addr) for addr in networkAddresses(ip, mask)]
    completed, pending = await asyncio.wait(coroutines)

    for task in pending:
        task.cancel()

    active = []
    for task in completed:
        res = task.result()
        if res:
            active.append(res)

    return active


async def main():
    args = dict([(idx, val) for idx, val in enumerate(sys.argv)])
    host, mask = (args.get(1, '192.168.1.0'), args.get(2, '255.255.255.0'))
    output_f = args.get(3, 'addrs.text')

    print('============')
    print('ICMP SCAN')
    print('============')
    icmp_scan = await scan(icmp, mask, host)
    '''
    print('============')
    print('UDP SCAN')
    print('============')
    udp_scan = await scan(udp, '255.255.255.0', '185.46.171.43')
    print('============')
    print('TCP SCAN')
    print('============')
    tcp_scan = await scan(udp, '255.255.255.0', '185.46.171.43')
    '''

    results = list(set(icmp_scan))
    print(results)
    print('ICMP: {}'.format(len(icmp_scan)))
    #print('UDP: {}'.format(len(udp_scan)))
    #print('TCP: {}'.format(len(tcp_scan)))
    print('Overall: {}'.format(len(results)))

    with open(output_f, 'w') as f:
        for addr in results:
            f.write(addr + '\n')
        print('Saved to', output_f)

if __name__ == '__main__':
    start_time = time.time()
    loop = asyncio.SelectorEventLoop()
    asyncio.set_event_loop(loop)
    try:
        loop.run_until_complete(main())
    finally:
        for task in asyncio.Task.all_tasks():
            task.cancel()
            loop.run_until_complete(task)
        loop.close()
    print("--- {} seconds ---".format(time.time() - start_time))
