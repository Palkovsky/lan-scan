import socket
import struct
import random
import asyncio
import errno

from checksum import checksum


'''
    buildICMPPacket(id, seq, octets)
'''
def buildICMPPacket(identifier = 0, seq = 1, octets = 8):
    # type(8), code(8), checksum(16), identifier(16), sequence number(16)
    header = struct.pack('bbHHH', 8, 0, 0, identifier, seq)
    data = struct.pack(octets * 'Q', *[random.randint(0, 2 << 64 - 1) for _ in range(octets)])
    cs = checksum(header + data)
    header = struct.pack('bbHHH', 8, 0, socket.htons(cs), identifier, seq)
    return header + data

def build_socket():
    s = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.setblocking(False)
    return s

SOCKET_POOL_SIZE = 510
total = 0
hits = 0
socket_pool = [build_socket() for _ in range(SOCKET_POOL_SIZE)]


'''
    icmp(addr, timeout, packets)
    Pings specified address with echo ICMP request.
'''
async def icmp(loop, addr):

    global total, hits

    while not socket_pool:
        await asyncio.sleep(1.5)

    s = socket_pool.pop()
    await loop.sock_connect(s, (addr, 1))

    total += 1

    packet_id = random.randint(0, 2 << 8)
    packet = buildICMPPacket(packet_id)
    await loop.sock_sendall(s, packet)

    try:
        res = await asyncio.wait_for(loop.sock_recv(s, 1024), 0.8)
    except asyncio.TimeoutError as e:
        #s.close()
        socket_pool.append(s)
        return None

    #s.close()
    socket_pool.append(s)

    icmp_header = res[20:28] # because socket.SOCK_RAW includes extra 20 bytes of IP header
    type, code, cs, p_id, sequence = struct.unpack('bbHHH', icmp_header)
    if p_id == packet_id:
        hits += 1
        print('ICMP', addr, 'responded -', hits, '/', total)
        return addr

    return None


async def tcp(loop, addr, port=81):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.setblocking(False)

        try:
            await loop.sock_connect(s, (addr, 3000))
            await loop.sock_sendall(s, 'CRACOVIA PANY'.encode())
            res = await loop.sock_recv(s, 1024)
        except socket.error as serr:
            if serr.errno != socket.errno.ECONNREFUSED:
                return None
            print('TCP', addr, 'refused')
            return addr #connection refused, so there is machine

        print('TCP', addr, 'responded')
        return addr

    return None


async def udp(loop, addr, ports=100):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.setblocking(False)
        s.bind(('', 0))

        ports = [2, 3, 5, 7, 9, 42, 44, 53, 80, 161, 162]
        for port in ports:
            await loop.sock_connect(s, (addr, port))
            try:
                await loop.sock_sendall(s, 'CRACOVIA PANY'.encode())
                res = await loop.sock_recv(s, 1024)
            except socket.error as serr:
                if serr.errno != socket.errno.ECONNREFUSED:
                    return None
                print('UDP', addr, 'refused')
                return addr #connection refused, so there is machine

            print('UDP', addr, 'responded')
            return addr

    return None
